﻿using System.Reflection;
using Abp.AutoMapper;
using Abp.Modules;

namespace Spotify
{
    /// <summary>
    /// 'Application layer module' for this project.
    /// </summary>
    [DependsOn(typeof(SpotifyCoreModule), typeof(AbpAutoMapperModule))]
    public class SpotifyApplicationModule : AbpModule
    {
        public override void Initialize()
        {
            //This code is used to register classes to dependency injection system for this assembly using conventions.
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());

            // We must declare mapping to be able to use AutoMapper
            DtoMappings.Map();
        }
    }
}
