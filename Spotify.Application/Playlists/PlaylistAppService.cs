﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Spotify.Playlists.DTOs;

namespace Spotify.Playlists
{
    public class PlaylistAppService : SpotifyAppServiceBase, IPlaylistAppService
    {
        private readonly IPlaylistRepository _playlistRepository;

        public PlaylistAppService(IPlaylistRepository playlistRepository)
        {
            _playlistRepository = playlistRepository;
        }

        public GetPlaylistsOutput GetAllPlaylists()
        {
            var playlists = _playlistRepository.GetAllList();

            return new GetPlaylistsOutput
            {
                Playlists = Mapper.Map<List<PlaylistDto>>(playlists)
            };
        }

        public PlaylistDto GetPlaylist(GetPlaylistInput input)
        {
            var playlist = _playlistRepository.FirstOrDefault(input.Id);

            return Mapper.Map<PlaylistDto>(playlist);
        }

        public void UpdatePlaylist(UpdatePlaylistInput input)
        {
            var playlist = _playlistRepository.Get(input.Id);

            playlist.Name = input.Name;
        }

        public void CreatePlaylist(CreatePlaylistInput input)
        {
            var playlist = new Playlist { Name = input.Name };

            _playlistRepository.Insert(playlist);
        }

        public async Task DeletePlaylist(DeletePlaylistInput input)
        {
            await _playlistRepository.DeleteAsync(input.Id);
        }
    }
}
