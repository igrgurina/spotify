﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace Spotify.Playlists.DTOs
{
    public class UpdatePlaylistInput : IInputDto
    {
        [Range(1, int.MaxValue)]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
