﻿using Abp.Application.Services.Dto;

namespace Spotify.Playlists.DTOs
{
    public class GetPlaylistInput : IInputDto
    {
        public int Id { get; set; }
    }
}
