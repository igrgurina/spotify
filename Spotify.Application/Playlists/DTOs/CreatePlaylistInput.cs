﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace Spotify.Playlists.DTOs
{
    public class CreatePlaylistInput : IInputDto
    {
        [Required]
        public string Name { get; set; }
    }
}
