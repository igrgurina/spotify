﻿using Abp.Application.Services.Dto;

namespace Spotify.Playlists.DTOs
{
    public class PlaylistDto : EntityDto
    {
        public string Name { get; set; }
        public bool? Checked { get; set; }
    }
}
