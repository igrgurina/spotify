﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace Spotify.Playlists.DTOs
{
    public class GetPlaylistsOutput : IOutputDto
    {
        public List<PlaylistDto> Playlists { get; set; }
    }
}
