﻿using Abp.Application.Services.Dto;

namespace Spotify.Playlists.DTOs
{
    public class DeletePlaylistInput : IInputDto
    {
        public int Id { get; set; }
    }
}
