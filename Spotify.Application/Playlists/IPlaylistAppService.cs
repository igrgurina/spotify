﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using Spotify.Playlists.DTOs;

namespace Spotify.Playlists
{
    public interface IPlaylistAppService : IApplicationService
    {
        GetPlaylistsOutput GetAllPlaylists();
        PlaylistDto GetPlaylist(GetPlaylistInput input);
        void UpdatePlaylist(UpdatePlaylistInput input);
        void CreatePlaylist(CreatePlaylistInput input);
        Task DeletePlaylist(DeletePlaylistInput input);
    }
}
