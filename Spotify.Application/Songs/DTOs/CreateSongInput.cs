﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;

namespace Spotify.Songs.DTOs
{
    public class CreateSongInput : IInputDto
    {
        [Required]
        public string Artist { get; set; }
        [Required]
        public string Title { get; set; }

        public override string ToString()
        {
            return string.Format("[CreateSongInput > Artist = {0}, Title = {1}]", Artist, Title);
        }
    }
}
