﻿using Abp.Application.Services.Dto;

namespace Spotify.Songs.DTOs
{
    public class GetSongInput : IInputDto
    {
        public long Id { get; set; }
    }
}
