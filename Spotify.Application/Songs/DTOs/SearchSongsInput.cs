﻿using Abp.Application.Services.Dto;

namespace Spotify.Songs.DTOs
{
    public class SearchSongsInput : IInputDto
    {
        public string PlaylistName { get; set; }

        public string Title { get; set; }

        public string Artist { get; set; }
    }
}
