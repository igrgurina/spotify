﻿using Abp.Application.Services.Dto;

namespace Spotify.Songs.DTOs
{
    public class DeleteSongInput : IInputDto
    {
        public long Id { get; set; }
    }
}
