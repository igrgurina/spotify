﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;

namespace Spotify.Songs.DTOs
{
    public class GetSongsOutput : IOutputDto
    {
        public List<SongDto> Songs { get; set; }
    }
}
