﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Spotify.Playlists.DTOs;

namespace Spotify.Songs.DTOs
{
    public class AddSongToPlaylistsInput : IInputDto
    {
        public long Id { get; set; } // song id
        public List<PlaylistDto> Playlists { get; set; }
    }
}
