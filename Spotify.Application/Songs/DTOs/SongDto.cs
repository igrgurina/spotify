﻿using Abp.Application.Services.Dto;

namespace Spotify.Songs.DTOs
{
    public class SongDto : EntityDto<long>
    {
        public string Title { get; set; }
        public string Artist { get; set; }

    }
}
