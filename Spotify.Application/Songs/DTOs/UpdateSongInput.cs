﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Abp.Application.Services.Dto;

namespace Spotify.Songs
{
    public class UpdateSongInput : IInputDto
    {
        [Range(1, long.MaxValue)]
        public long Id { get; set; }

        [Required]
        public string Artist { get; set; }

        [Required]
        public string Title { get; set; }

        public override string ToString()
        {
            return string.Format("[UpdateSongInput > Id = {0}, Artist = {1}, Title = {2}]", Id, Artist, Title);
        }
    }
}
