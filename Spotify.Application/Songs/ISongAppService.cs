﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Spotify.Playlists.DTOs;
using Spotify.Songs.DTOs;

namespace Spotify.Songs
{
    public interface ISongAppService : IApplicationService
    {
        SongDto GetSong(GetSongInput id);
        GetSongsOutput GetAllSongs();
        void UpdateSong(UpdateSongInput input);
        void CreateSong(CreateSongInput input);
        GetSongsOutput SearchSongs(SearchSongsInput input);
        Task DeleteSong(DeleteSongInput input);
        GetPlaylistsOutput GetPlaylistsForSong(GetSongInput input);
        void AddSongToPlaylists(AddSongToPlaylistsInput input);
    }
}