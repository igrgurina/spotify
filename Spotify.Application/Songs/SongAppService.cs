﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Castle.Components.DictionaryAdapter;
using Spotify.Playlists;
using Spotify.Playlists.DTOs;
using Spotify.Songs.DTOs;

namespace Spotify.Songs
{
    public class SongAppService : SpotifyAppServiceBase, ISongAppService
    {
        private readonly ISongRepository _songRepository;
        private readonly IPlaylistRepository _playlistRepository;

        public SongAppService(ISongRepository songRepository, IPlaylistRepository playlistRepository)
        {
            _songRepository = songRepository;
            _playlistRepository = playlistRepository;
        }

        public async Task DeleteSong(DeleteSongInput input)
        {
            await _songRepository.DeleteAsync(input.Id);
        }

        public SongDto GetSong(GetSongInput input)
        {
            var song = _songRepository.Get(input.Id);

            return Mapper.Map<SongDto>(song);
        }

        public GetSongsOutput GetAllSongs()
        {
            var songs = _songRepository.GetAllList();

            return new GetSongsOutput
            {
                Songs = Mapper.Map<List<SongDto>>(songs)
            };
        }

        public GetSongsOutput SearchSongs(SearchSongsInput input)
        {
            var songs = _songRepository.SearchAll(
                songTitle: input.Title,
                songArtist: input.Artist,
                playlistName: input.PlaylistName);

            return new GetSongsOutput
            {
                Songs = Mapper.Map<List<SongDto>>(songs)
            };
        }

        public void UpdateSong(UpdateSongInput input)
        {
            var song = _songRepository.Get(input.Id);

            song.Artist = input.Artist;
            song.Title = input.Title;
        }

        public void CreateSong(CreateSongInput input)
        {
            var song = new Song { Title = input.Title, Artist = input.Artist };

            _songRepository.Insert(song);
        }

        public GetPlaylistsOutput GetPlaylistsForSong(GetSongInput input)
        {
            long songId = input.Id;
            var song = _songRepository.Get(songId);

            var allPlaylists = _playlistRepository.GetAllList();

            var playlists = allPlaylists.Select(playlist => new PlaylistDto
            {
                Name = playlist.Name,
                Checked = playlist.Songs.Contains(song)
            }).ToList();

            return new GetPlaylistsOutput
            {
                Playlists = playlists
            };
        }

        // add/remove song to/from playlist(s)
        public void AddSongToPlaylists(AddSongToPlaylistsInput input)
        {
            long songId = input.Id;
            var song = _songRepository.Get(songId);

            // ReSharper disable once PossibleInvalidOperationException
            foreach (var playlist in input.Playlists)//.Where(playlist => playlist.Checked.Value))
            {
                var pom = playlist;
                song.Playlists.Add(_playlistRepository.FirstOrDefault(p => p.Name == pom.Name));
                _songRepository.InsertOrUpdate(song);
            }


        }
    }
}