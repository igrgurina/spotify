﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Spotify.Playlists;
using Spotify.Playlists.DTOs;
using Spotify.Songs;
using Spotify.Songs.DTOs;

namespace Spotify
{
    internal static class DtoMappings
    {
        public static void Map()
        {
            //Mapper.CreateMap<Task, TaskDto>().ForMember(t => t.AssignedPersonId, opts => opts.MapFrom(d => d.AssignedPerson.Id));
            Mapper.CreateMap<Song, SongDto>();
            Mapper.CreateMap<SongDto, Song>();
            Mapper.CreateMap<Playlist, PlaylistDto>();
            Mapper.CreateMap<PlaylistDto, Playlist>();
        }
    }
}
