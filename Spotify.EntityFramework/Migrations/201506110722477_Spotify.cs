namespace Spotify.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Spotify : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Playlists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Songs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Artist = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SongPlaylists",
                c => new
                    {
                        Song_Id = c.Long(nullable: false),
                        Playlist_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Song_Id, t.Playlist_Id })
                .ForeignKey("dbo.Songs", t => t.Song_Id, cascadeDelete: true)
                .ForeignKey("dbo.Playlists", t => t.Playlist_Id, cascadeDelete: true)
                .Index(t => t.Song_Id)
                .Index(t => t.Playlist_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SongPlaylists", "Playlist_Id", "dbo.Playlists");
            DropForeignKey("dbo.SongPlaylists", "Song_Id", "dbo.Songs");
            DropIndex("dbo.SongPlaylists", new[] { "Playlist_Id" });
            DropIndex("dbo.SongPlaylists", new[] { "Song_Id" });
            DropTable("dbo.SongPlaylists");
            DropTable("dbo.Songs");
            DropTable("dbo.Playlists");
        }
    }
}
