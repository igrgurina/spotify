using Spotify.EntityFramework;
using Spotify.Playlists;
using Spotify.Songs;

namespace Spotify.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SpotifyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Spotify";
        }

        protected override void Seed(SpotifyDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...
            //context.People.AddOrUpdate(
            //p => p.Name,
            //new Person { Name = "Isaac Asimov" },
            //new Person { Name = "Thomas More" },
            //new Person { Name = "George Orwell" },
            //new Person { Name = "Douglas Adams" }
            //);

            //context.Tasks.AddOrUpdate(
            //    new Task { Description = "Test", State = TaskState.Active, CreationTime = DateTime.Now },
            //    new Task { Description = "Test 1", State = TaskState.Active, CreationTime = DateTime.Now },
            //    new Task { Description = "Test 2", State = TaskState.Active, CreationTime = DateTime.Now },
            //    new Task { Description = "Test 3", State = TaskState.Active, CreationTime = DateTime.Now },
            //    new Task { Description = "Test 4", State = TaskState.Active, CreationTime = DateTime.Now },
            //    new Task { Description = "Test 5", State = TaskState.Active, CreationTime = DateTime.Now },
            //    new Task { Description = "Test 6", State = TaskState.Active, CreationTime = DateTime.Now }
            //    );

            context.Songs.AddOrUpdate(
                new Song { Artist = "Eminem", Title = "Not Afraid" }
                );

            context.Playlists.AddOrUpdate(
                new Playlist { Name = "Morning Coffee" }
                );
        }
    }
}
