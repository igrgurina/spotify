﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.EntityFramework;
using Spotify.Playlists;

namespace Spotify.EntityFramework.Repositories
{
    class PlaylistRepository : SpotifyRepositoryBase<Playlist>, IPlaylistRepository
    {
        public PlaylistRepository(IDbContextProvider<SpotifyDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }
    }
}
