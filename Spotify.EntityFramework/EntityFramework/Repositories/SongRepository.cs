﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Abp.EntityFramework;
using Abp.Extensions;
using Spotify.Songs;

namespace Spotify.EntityFramework.Repositories
{
    public class SongRepository : SpotifyRepositoryBase<Song, long>, ISongRepository
    {
        public SongRepository(IDbContextProvider<SpotifyDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
        }

        public List<Song> SearchAll(string songTitle, string songArtist, string playlistName)
        {
            var query = GetAll();

            if (!string.IsNullOrEmpty(songTitle))
            {
                query = query.Where(song => song.Title.ToLower().Contains(songTitle.ToLower()));
            }

            if (!string.IsNullOrEmpty(songArtist))
            {
                query = query.Where(song => song.Artist.ToLower().Contains(songArtist.ToLower()));
            }

            if (!string.IsNullOrEmpty(playlistName))
            {
                query = query.Where(song => song.Playlists.Any(o => o.Name.ToLower().Contains(playlistName.ToLower())));
            }

            return query
                .Include(song => song.Playlists)
                .ToList();
        }
    }
}