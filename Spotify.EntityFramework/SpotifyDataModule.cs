﻿using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;
using Spotify.EntityFramework;
using Spotify.Playlists;
using Spotify.Songs;

namespace Spotify
{
    [DependsOn(typeof(AbpEntityFrameworkModule), typeof(SpotifyCoreModule))]
    public class SpotifyDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<SpotifyDbContext>(new SpotifyDbInitializer());
        }
    }

    public class SpotifyDbInitializer : CreateDatabaseIfNotExists<SpotifyDbContext>
    {
        protected override void Seed(SpotifyDbContext context)
        {
            context.Songs.Add(
                new Song { Artist = "Eminem", Title = "Not Afraid" }
                );

            context.Playlists.Add(
                new Playlist { Name = "Morning Coffee" }
                );

            context.SaveChanges();
        }
    }
}
