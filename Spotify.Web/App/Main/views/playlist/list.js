﻿(function () {
    var app = angular.module('app');

    var controllerId = 'sts.views.playlist.list';
    app.controller(controllerId, [
        '$scope', '$location', 'abp.services.spotify.playlist',
        function ($scope, $location, playlistService) {
            var vm = this;

            vm.localize = abp.localization.getSource('Spotify');

            vm.playlists = [];

            vm.getPlaylists = function () {
                playlistService.getAllPlaylists().success(function (data) {
                    vm.playlists = data.playlists;
                });
            }

            vm.getPlaylists(); // the initial data load

            vm.getPlaylistCountText = function () {
                return abp.utils.formatString(vm.localize('Xplaylists'), vm.playlists.length);
            }

            $scope.editPlaylist = function (id) {
                $location.path("/playlist/edit/" + id);
            };
            $scope.deletePlaylist = function (id) {
                abp.ui.setBusy(
                   null,
                   playlistService.deletePlaylist({
                       Id: id
                   }).success(function () {
                       vm.getPlaylists(); // refresh view - load changed data
                   })
               );

                // notify the user
                abp.notify.info('', vm.localize('PlaylistDeletedMessage'));
            };
        }
    ]);
})();