﻿(function () {
    var app = angular.module('app');

    var controllerId = 'sts.views.playlist.edit';
    app.controller(controllerId, [
        '$scope', '$stateParams', '$location', 'abp.services.spotify.playlist',
        function ($scope, $stateParams, $location, playlistService) {
            var vm = this;

            var localize = abp.localization.getSource('Spotify');

            var playlistId = $stateParams.id;

            console.log(playlistId);

            vm.playlist = {
                id: playlistId,
                name: ''
            };

            playlistService.getPlaylist({ Id: playlistId })
                .success(function (result) {
                    console.log(result);
                    vm.playlist = result;
                });

            vm.savePlaylist = function () {
                abp.ui.setBusy(
                    null,
                    playlistService.updatePlaylist(
                        vm.playlist
                    ).success(function () {
                        abp.notify.info(abp.utils.formatString(localize("PlaylistUpdatedMessage"), vm.playlist.name));
                        $location.path('/playlist');
                    })
                );
            };
        }
    ]);
})();