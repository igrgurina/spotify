﻿(function () {
    var app = angular.module('app');

    var controllerId = 'sts.views.playlist.new';
    app.controller(controllerId, [
        '$scope', '$location', 'abp.services.spotify.playlist',
        function ($scope, $location, playlistService) {
            var vm = this;

            vm.playlist = {
                name: ''
            };

            var localize = abp.localization.getSource('Spotify');

            vm.savePlaylist = function () {
                abp.ui.setBusy(
                    null,
                    playlistService.createPlaylist(
                        vm.playlist
                    ).success(function () {
                        abp.notify.info(abp.utils.formatString(localize("PlaylistCreatedMessage"), vm.playlist.name));
                        $location.path('/');
                    })
                );
            };
        }
    ]);
})();