﻿(function () {
    var app = angular.module('app');

    var controllerId = 'sts.views.song.edit';
    app.controller(controllerId, [
        '$scope', '$stateParams', '$location', 'abp.services.spotify.song',
        function ($scope, $stateParams, $location, songService) {
            var vm = this;

            var localize = abp.localization.getSource('Spotify');

            var songId = $stateParams.id;

            console.log(songId);

            vm.song = {
                id: songId,
                artist: '',
                title: ''
            };

            songService.getSong({ Id: songId })
                .success(function (result) {
                    console.log(result);
                    vm.song = result;
                });

            vm.saveSong = function () {
                abp.ui.setBusy(
                    null,
                    songService.updateSong(
                        vm.song
                    ).success(function () {
                        abp.notify.info(abp.utils.formatString(localize("SongCreatedMessage"), vm.song.title));
                        $location.path('/');
                    })
                );
            };
        }
    ]);
})();