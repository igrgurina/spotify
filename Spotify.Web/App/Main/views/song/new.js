﻿(function () {
    var app = angular.module('app');

    var controllerId = 'sts.views.song.new';
    app.controller(controllerId, [
        '$scope', '$location', 'abp.services.spotify.song',
        function ($scope, $location, songService) {
            var vm = this;

            vm.song = {
                title: '',
                artist: ''
            };

            var localize = abp.localization.getSource('Spotify');

            vm.saveSong = function () {
                abp.ui.setBusy(
                    null,
                    songService.createSong(
                        vm.song
                    ).success(function () {
                        abp.notify.info(abp.utils.formatString(localize("SongCreatedMessage"), vm.song.title));
                        $location.path('/');
                    })
                );
            };
        }
    ]);
})();