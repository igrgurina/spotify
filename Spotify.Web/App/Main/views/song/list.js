﻿(function () {
    var app = angular.module('app');

    var controllerId = 'sts.views.song.list';
    app.controller(controllerId, [
        '$scope', '$location', 'abp.services.spotify.song',
        function ($scope, $location, songService) {
            var vm = this;

            vm.localize = abp.localization.getSource('Spotify');

            vm.songs = [];

            vm.getSongs = function () {
                songService.getAllSongs().success(function (data) {
                    vm.songs = data.songs;
                });
            }

            vm.getSongs(); // the initial data load

            vm.getSongCountText = function () {
                return abp.utils.formatString(vm.localize('Xsongs'), vm.songs.length);
            }


            $scope.addSongToPlaylists = function (id) {
                $location.path("/song/playlists/" + id);
            }

            $scope.editSong = function (id) {
                $location.path("/song/edit/" + id);
            };
            $scope.deleteSong = function (id) {
                abp.ui.setBusy(
                   null,
                   songService.deleteSong({
                       Id: id
                   }).success(function () {
                       vm.getSongs(); // refresh view - load changed data
                   })
               );
                abp.notify.info('', vm.localize('PlaylistDeletedMessage'));

            };
        }
    ]);
})();