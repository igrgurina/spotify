﻿(function () {
    var app = angular.module('app');

    var controllerId = 'sts.views.song.playlists.add';
    app.controller(controllerId, [
        '$scope', '$stateParams', '$location', 'abp.services.spotify.song', 'abp.services.spotify.playlist',
        function ($scope, $stateParams, $location, songService, playlistService) {
            var vm = this;

            vm.localize = abp.localization.getSource('Spotify');

            $scope.songId = $stateParams.id;

            $scope.playlists = [
                {
                    name: 'Loading...',
                    checked: true
                }
            ];

            $scope.getPlaylists = function () {
                abp.ui.setBusy(
                    null,
                    songService.getPlaylistsForSong({
                        Id: $scope.songId
                    }).success(function (data) {
                        $scope.playlists = data.playlists;
                    })
                );
            }();

            //$scope.getPlaylists(); // the initial data load
            //playlist = {name, checked}

            $scope.outputPlaylists = [];

            // song -> playlist(ticked=true); other(ticked=false);
            // onSave -> song.addSongToPlaylists(where ticked=true);


            $scope.save = function () {
                abp.ui.setBusy(
                   null,
                   songService.addSongToPlaylists({
                       Id: $scope.songId,
                       Playlists: $scope.outputPlaylists
                   }).success(function () {
                       abp.notify.success('', vm.localize('SongAddedToPlaylistsMessage'));
                       $location.path('/');
                   })
               );
                //abp.notify.info('', vm.localize('PlaylistDeletedMessage'));

            };
        }
    ]);
})();