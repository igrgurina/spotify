﻿(function () {
    var app = angular.module('app');

    var controllerId = 'sts.views.song.search';
    app.controller(controllerId, [
        '$scope', '$location', 'abp.services.spotify.song',
        function ($scope, $location, songService) {
            var vm = this;

            vm.localize = abp.localization.getSource('Spotify');

            vm.songs = [];

            vm.getSongCountText = function () {
                return abp.utils.formatString(vm.localize('Xsongs'), vm.songs.length);
            }

            vm.song = {
                playlist: '',
                title: '',
                name: ''
            }

            $scope.search = function () {
                abp.ui.setBusy(
                   null,
                   songService.searchSongs({
                       PlaylistName: vm.song.playlist,
                       Title: vm.song.title,
                       Artist: vm.song.artist
                   }).success(function (data) {
                       vm.songs = data.songs; // refresh view - load changed data
                   })
               );
            };
        }
    ]);
})();