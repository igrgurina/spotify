﻿(function () {
    'use strict';

    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',

        'isteven-multi-select', // multiselect dropdown list

        'abp'
    ]);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            $stateProvider
                //.state('home', {
                //    url: '/',
                //    templateUrl: '/App/Main/views/home/home.cshtml',
                //    menu: 'Home' //Matches to name of 'Home' menu in SpotifyNavigationProvider
                //})
                //.state('about', {
                //    url: '/about',
                //    templateUrl: '/App/Main/views/about/about.cshtml',
                //    menu: 'About' //Matches to name of 'About' menu in SpotifyNavigationProvider
                //});
                //.state('tasklist', {
                //    url: '/',
                //    templateUrl: '/App/Main/views/task/list.cshtml',
                //    menu: 'TaskList' // Matches to name of 'TaskList' menu in SpotifyNavigationProvider
                //})
                .state('songlist', {
                    url: '/',
                    templateUrl: '/App/Main/views/song/list.cshtml',
                    menu: 'SongList' // Matches to name of 'SongList' menu in SpotifyNavigationProvider
                })
                .state('newsong', {
                    url: '^/song/new',
                    templateUrl: '/App/Main/views/song/new.cshtml',
                    menu: 'NewSong' // Matches to name of 'NewSong' menu in SpotifyNavigationProvider
                })
                .state('editsong', {
                    url: '/song/edit/:id',
                    templateUrl: '/App/Main/views/song/edit.cshtml',
                    menu: 'EditSong' // Matches to name of 'EditSong' menu in SpotifyNavigationProvider
                })
                .state('addsongtoplaylists', {
                    url: '/song/playlists/:id',
                    templateUrl: '/App/Main/views/song/playlists.cshtml',
                    menu: 'AddSongToPlaylists' // Matches to name of 'EditSong' menu in SpotifyNavigationProvider
                })
                .state('songsearch', {
                    url: '/search',
                    templateUrl: '/App/Main/views/song/search.cshtml',
                    menu: 'SongSearch' // Matches to name of 'EditSong' menu in SpotifyNavigationProvider
                })
                .state('playlistlist', {
                    url: '/playlist',
                    templateUrl: '/App/Main/views/playlist/list.cshtml',
                    menu: 'PlaylistList' // Matches to name of 'PlaylistList' menu in SpotifyNavigationProvider
                })
                .state('newplaylist', {
                    url: '/playlist/new',
                    templateUrl: '/App/Main/views/playlist/new.cshtml',
                    menu: 'NewPlaylist' // Matches to name of 'NewPlaylist' menu in SpotifyNavigationProvider
                })
            .state('editplaylist', {
                url: '/playlist/edit/:id',
                templateUrl: '/App/Main/views/playlist/edit.cshtml',
                menu: 'EditPlaylist' // Matches to name of 'EditPlaylist' menu in SpotifyNavigationProvider
            });
        }
    ]);
})();