﻿using System.Collections.Generic;
using Abp.Domain.Repositories;

namespace Spotify.Songs
{
    public interface ISongRepository : IRepository<Song, long>
    {
        List<Song> SearchAll(string songTitle, string songArtist, string playlistName);
    }
}