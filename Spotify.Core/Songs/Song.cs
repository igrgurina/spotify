﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Spotify.Playlists;

namespace Spotify.Songs
{
    public class Song : Entity<long>
    {
        [Required]
        public virtual string Title { get; set; }
        [Required]
        public virtual string Artist { get; set; }

        public virtual List<Playlist> Playlists { get; set; }

        public Song()
        {
            Playlists = new List<Playlist>();
        }
    }
}
