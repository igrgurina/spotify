﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Abp.Domain.Entities;
using Spotify.Songs;

namespace Spotify.Playlists
{
    public class Playlist : Entity
    {
        [Required]
        public virtual string Name { get; set; }

        public virtual List<Song> Songs { get; set; }

        public Playlist()
        {
            this.Songs = new List<Song>();
        }
    }
}
